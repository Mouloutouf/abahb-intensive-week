using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeakPlatform : MonoBehaviour
{
    public GameObject locket;

    bool wasActivate = false;

    // Update is called once per frame
    void Update()
    {
        if (!wasActivate)
        {
            if (locket.GetComponent<LockHit>().isOpen)
            {
                wasActivate = true;
                StartCoroutine(DestroyAfterTime());
            }
        }
    }

    IEnumerator DestroyAfterTime()
    {
        yield return new WaitForSeconds(1f);
        this.gameObject.SetActive(false);
    }
}
