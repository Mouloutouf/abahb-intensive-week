using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightScript : MonoBehaviour
{

    [SerializeField] GameObject Nut;

    void Start()
    {
        StartCoroutine("LightFlashing");
    }

    IEnumerator LightFlashing()
    {
        yield return new WaitForSeconds(0.1f);
        GetComponent<Light>().enabled = false;
        yield return new WaitForSeconds(0.1f);
        GetComponent<Light>().enabled = true;
        yield return new WaitForSeconds(0.2f);
        GetComponent<Light>().enabled = false;
        yield return new WaitForSeconds(0.4f);
        GetComponent<Light>().enabled = true;
        yield return new WaitForSeconds(0.1f);
        GetComponent<Light>().enabled = false;
        Nut.SetActive(true);
    }
}