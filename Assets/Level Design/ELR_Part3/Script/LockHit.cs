using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class LockHit : HitBehaviour
{
    public Animator animatorToActivate;

    [SerializeField] float animationSpeedMultiplier = 1f;
    [SerializeField] Vector3 forceToApplyOnLock;

    public bool isOpen = false;

    public VisualEffectAsset sparkFXAsset;

    

    public override void OnHit()
    {
        animatorToActivate.SetTrigger("Activate");
        animatorToActivate.SetFloat("SpeedMultiplier", animationSpeedMultiplier);

        GetComponent<VisualEffect>().visualEffectAsset = sparkFXAsset;

        isOpen = true;

        GetComponent<Rigidbody>().isKinematic = false;
        GetComponent<Rigidbody>().useGravity = true;
        GetComponent<Rigidbody>().AddForce(forceToApplyOnLock);

        this.enabled = false;
    }
}
