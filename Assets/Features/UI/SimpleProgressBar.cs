using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

public class SimpleProgressBar : ProgressBar
{
    [SerializeField, FoldoutGroup("References"), HideIf("lockReferences")]
    private RectTransform barAnchor;
    [SerializeField, FoldoutGroup("References"), HideIf("lockReferences")]
    private Image barFill;
    [SerializeField, FoldoutGroup("Bar Settings")]
    private float barMaxWidth;

    [Space, SerializeField]
    private bool lockReferences;
    
    protected override void UpdateMax()
    {
        barAnchor.sizeDelta = new Vector2(barMaxWidth * (MaxValue / (float) AbsoluteMaxValue), barAnchor.sizeDelta.y);
        
        UpdateBar(true);
    }

    protected override void UpdateBar(bool immediate = false)
    {
        if (immediate)
        {
            VisualValue = Value;
            return;
        }
        barFill.fillAmount = VisualValue / (float) MaxValue;
    }
}
