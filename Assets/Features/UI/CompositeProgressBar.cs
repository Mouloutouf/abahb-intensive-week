using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

public class CompositeProgressBar : MonoBehaviour
{
    [PropertyRange(0, 1), ShowInInspector, HideInEditorMode]
    public float fillAmount
    {
        get => _fillAmount;
        set { _fillAmount = Mathf.Clamp(value, 0, 1);
            absoluteFillAmount = _fillAmount * AbsoluteMaxFillAmount;
        }
    }
    private float _fillAmount;
    
    private float absoluteFillAmount
    {
        get => _absoluteFillAmount;
        set { _absoluteFillAmount = Mathf.Clamp(value, 0, AbsoluteMaxFillAmount);
            FillAmountChanged(_absoluteFillAmount);
        }
    }
    private float _absoluteFillAmount;
    
    private float AbsoluteMaxFillAmount;
    private bool init;
    
    [SerializeField] private List<ProgressBar> bars = new List<ProgressBar>();
    // Update is called once per frame
    void Start()
    {
        AbsoluteMaxFillAmount = bars.Sum(b => b.MaxRange);
        init = true;
    }
    
    private void FillAmountChanged(float value)
    {
        var remainingFillAmount = value;
        foreach (var bar in bars)
        {
            var availableAmount = Mathf.Min(bar.MaxRange, remainingFillAmount);
            bar.Image.fillAmount = availableAmount;
            remainingFillAmount = Mathf.Clamp(remainingFillAmount - availableAmount, 0, AbsoluteMaxFillAmount);
        }
    }
    
    [Serializable]
    private class ProgressBar
    {
        public Image Image;
        public float MaxRange;
    }
}
