using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

public abstract class ProgressBar : MonoBehaviour
{
    [SerializeField, FoldoutGroup("Bar Settings")]
    private float fillRate = 10;
    
    [TitleGroup("Values")]
    [ShowInInspector, PropertyRange(0, "_maxValue")]
    public int Value
    {
        get => _value;
        set
        {
            var newVal = Mathf.Clamp(value, 0, MaxValue);
            if (newVal != _value)
            {
                PrevValue = _value;
                _value = newVal;
                if(!Application.isPlaying) UpdateBar(true);
                OnValueChanged.Invoke(_value, PrevValue);
            }
        }
    }

    [SerializeField, HideInInspector] 
    private int _value;

    public float Percentage
    {
        get => Value / (float) MaxValue;
        set
        {
            Value = Mathf.FloorToInt(MaxValue * value);
        }
    }

    public int PrevValue { get; set; }

    [ShowInInspector, ReadOnly, HideInEditorMode]
    public float VisualValue
    {
        get => _visualValue;
        set { _visualValue = Mathf.Clamp(value, 0, MaxValue); UpdateBar(); }
    }
    private float _visualValue = 20;

    [ShowInInspector]
    public int MaxValue
    {
        get => _maxValue;
        set { 
            var newVal = Mathf.Clamp(value, 0, absoluteMaxValue);
            if (newVal != _maxValue)
            {
                PrevMaxValue = _maxValue;
                _maxValue = newVal;
                if(VisualValue > _maxValue) VisualValue = _maxValue;
                if (Value > _maxValue) Value = _maxValue; 
                UpdateMax();
                OnMaxValueChanged.Invoke(_maxValue, PrevMaxValue);
            }
        }
    }
    [SerializeField, HideInInspector]
    private int _maxValue = 20;

    public int PrevMaxValue { get; set; }
    
    protected virtual bool absoluteMaxValueAsProperty => false;

    [ShowInInspector, HideIf("absoluteMaxValueAsProperty")]
    public int AbsoluteMaxValue
    {
        get => absoluteMaxValueAsProperty ? absoluteMaxValue : _absoluteMaxValue;
        set
        {
            if (!absoluteMaxValueAsProperty)
            {
                _absoluteMaxValue = value;
                if (_maxValue > _absoluteMaxValue) _maxValue = _absoluteMaxValue;
            }
        }
    }
    protected virtual int absoluteMaxValue => absoluteMaxValueAsProperty ? 200 : _absoluteMaxValue;
    [SerializeField, HideInInspector]
    private int _absoluteMaxValue = 200;
    
    private float targetValue;

    [FoldoutGroup("Events"), SerializeField]
    private UnityEvent<int, int> OnValueChanged;
    [FoldoutGroup("Events"), SerializeField]
    private UnityEvent<int, int> OnMaxValueChanged;

    protected abstract void UpdateMax();

    protected abstract void UpdateBar(bool immediate = false);


    private void Update()
    {
        if (Value != VisualValue)
        {
            var sign = Mathf.Sign(Value - VisualValue);
            VisualValue = Mathf.Clamp(VisualValue + sign * fillRate * 10 * Time.deltaTime,
                sign == 1 ? 0 : Value, sign == 1 ? Value : MaxValue);
        }
    }
}
