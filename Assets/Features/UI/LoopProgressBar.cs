using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class LoopProgressBar : ProgressBar
{
    [SerializeField, FoldoutGroup("References"), HideIf("lockReferences")]
    private Image radialBorder;
    [SerializeField, FoldoutGroup("References"), HideIf("lockReferences")]
    private Image radialEmpty;
    [SerializeField, FoldoutGroup("References"), HideIf("lockReferences")]
    private Image radialFill;
    [SerializeField, FoldoutGroup("References"), HideIf("lockReferences")]
    private RectTransform barAnchor;
    [SerializeField, FoldoutGroup("References"), HideIf("lockReferences")]
    private Image barFill;
    
    [SerializeField, FoldoutGroup("Bar Settings")]
    private int radialMaxValue = 100;
    [SerializeField, FoldoutGroup("Bar Settings")]
    private float barMaxWidth = 500;
    [SerializeField, FoldoutGroup("Bar Settings")]
    private int barMaxValue = 100;
    [SerializeField, FoldoutGroup("Bar Settings")]
    private float radialMaxFill = 0.75f;
    [SerializeField, FoldoutGroup("Bar Settings")]
    private float radialFillOffset = 0.06f;

    protected override bool absoluteMaxValueAsProperty => true;
    [ShowInInspector, ShowIf("absoluteMaxValueAsProperty"), ReadOnly]
    protected override int absoluteMaxValue => radialMaxValue + barMaxValue;

    [Space, SerializeField]
    private bool lockReferences;

    protected override void UpdateMax()
    {
        if (MaxValue >= radialMaxValue)
        {
            radialBorder.fillAmount = radialMaxFill;
            radialEmpty.fillAmount = radialMaxFill;

            if (MaxValue > radialMaxValue)
            {
                var remainingValue = MaxValue - radialMaxValue;
                barAnchor.sizeDelta = new Vector2(barMaxWidth * (remainingValue/(float)barMaxValue), barAnchor.sizeDelta.y);
                if(!barAnchor.gameObject.activeSelf) barAnchor.gameObject.SetActive(true);
            }
        }
        else
        {
            var ratio = radialMaxFill * (MaxValue /(float)radialMaxValue);
            radialBorder.fillAmount = ratio + radialFillOffset;
            radialEmpty.fillAmount = ratio;
            if (radialFill.fillAmount > ratio) radialFill.fillAmount = ratio;
            
            if(barAnchor.gameObject.activeSelf) barAnchor.gameObject.SetActive(false);
        }
        UpdateBar(true);
    }

    protected override void UpdateBar(bool immediate = false)
    {
        if (immediate)
        {
            VisualValue = Value;
            return;
        }
        
        radialFill.fillAmount = VisualValue >= radialMaxValue ? radialMaxFill : radialMaxFill * (VisualValue / radialMaxValue);
        var remainingValue = VisualValue - radialMaxValue;
        barFill.fillAmount = remainingValue/(float)(MaxValue-radialMaxValue);
    }
}
