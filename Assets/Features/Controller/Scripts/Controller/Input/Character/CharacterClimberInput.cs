﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace CMF
{
    public class CharacterClimberInput : CharacterInput
    {
        public Movement axises = Movement.Horizontal;
        public string horizontalInputAxis = "Horizontal";
        public string verticalInputAxis = "Vertical";
        public KeyCode jumpKey = KeyCode.Space;
        public KeyCode dashKey = KeyCode.LeftControl;

        [NonSerialized]
        public bool freeze = false;
        //If this is enabled, Unity's internal input smoothing is bypassed;
        public bool useRawInput = true;
        
        [NonSerialized, ShowInInspector]
        public DirectionalKeys directionalKeys = new DirectionalKeys();

        private void Update()
        {
            directionalKeys.Evaluate(Vertical(), Horizontal());
        }

        public override float GetHorizontalMovementInput()
        {
            if (freeze || !axises.HasFlag(Movement.Horizontal)) return 0;
            if(useRawInput)
                return Input.GetAxisRaw(horizontalInputAxis);
            else
                return Input.GetAxis(horizontalInputAxis);
        }

        public override float GetVerticalMovementInput()
        {
            if (freeze || !axises.HasFlag(Movement.Vertical)) return 0;
            if(useRawInput)
                return Input.GetAxisRaw(verticalInputAxis);
            else
                return Input.GetAxis(verticalInputAxis);
        }

        public float Horizontal()
        {
            return Input.GetAxisRaw(horizontalInputAxis);
        }
        public float Vertical()
        {
            return Input.GetAxisRaw(verticalInputAxis);
        }

        public bool Jump()
        {
            return Input.GetKey(jumpKey);
        }

        public override bool IsJumpKeyPressed()
        {
            if (freeze || axises.HasFlag(Movement.Vertical)) return false;
            return Input.GetKey(jumpKey);
        }
        
        public bool IsDashKeyPressed()
        {
            if (freeze || axises.HasFlag(Movement.Vertical)) return false;
            return Input.GetKey(dashKey);
        }
        
        [Flags]
        public enum Movement
        {
            None = 0,
            Horizontal = 1,
            Vertical = 2,
        }
    }
}