using System;
using System.Collections;
using System.Collections.Generic;
using CMF;
using Sirenix.OdinInspector;
using UnityEngine;

public class Character2DInput : CharacterInput
{
    public Movement axises = Movement.Horizontal;
    public string horizontalInputAxis = "Horizontal";
    public string verticalInputAxis = "Vertical";
    public KeyCode jumpKey = KeyCode.Space;
    public bool inverse = false;
    public bool freeze = false;
    //If this is enabled, Unity's internal input smoothing is bypassed;
    public bool useRawInput = true;

    public override float GetHorizontalMovementInput()
    {
        if (freeze || !axises.HasFlag(Movement.Horizontal)) return 0;
        if(useRawInput)
            return Input.GetAxisRaw(inverse ? verticalInputAxis : horizontalInputAxis);
        else
            return Input.GetAxis(inverse ? verticalInputAxis : horizontalInputAxis);
    }

    public override float GetVerticalMovementInput()
    {
        if (freeze || !axises.HasFlag(Movement.Vertical)) return 0;
        if(useRawInput)
            return Input.GetAxisRaw(inverse ? horizontalInputAxis : verticalInputAxis);
        else
            return Input.GetAxis(inverse ? horizontalInputAxis : verticalInputAxis);
    }

    public float Horizontal()
    {
        return Input.GetAxisRaw(inverse ? verticalInputAxis : horizontalInputAxis);
    }
    public float Vertical()
    {
        return Input.GetAxisRaw(inverse ? horizontalInputAxis : verticalInputAxis);
    }

    public override bool IsJumpKeyPressed()
    {
        if (freeze || axises.HasFlag(Movement.Horizontal) && axises.HasFlag(Movement.Vertical)) return false;
        return Input.GetKey(jumpKey);
    }

    [Flags]
    public enum Movement
    {
        None = 0,
        Horizontal = 1,
        Vertical = 2,
    }
}