using System;
using System.Collections;
using System.Collections.Generic;
using CMF;
using UnityEngine;

public class StickToGround : MonoBehaviour
{
    [SerializeField] 
    private float smoothSpeed = 10;
    [SerializeField] 
    private float changeSurfaceDuration = 1;
    
    private Mover mover;
    private AdvancedWalkerController walker;
    private Rigidbody rb;
    private Collider col;
    private bool grounded;
    private bool prevGrounded;
    private bool switchingWall;

    private void Start()
    {
        TryGetComponent(out mover);
        TryGetComponent(out walker);
        TryGetComponent(out rb);
        TryGetComponent(out col);
    }

    private void FixedUpdate()
    {
        if(switchingWall) return;
        prevGrounded = grounded;
        grounded = mover.IsGrounded();
        var normal = mover.GetGroundNormal();
        if (grounded)
        {
            if (CheckFacingWall(normal));
            else rb.MoveRotation(Quaternion.Lerp(rb.rotation, GetRotationFromNormal(normal), smoothSpeed * Time.deltaTime));
        }
        else rb.MoveRotation(Quaternion.Lerp(rb.rotation, Quaternion.identity, smoothSpeed * Time.deltaTime));
    }

    private bool CheckFacingWall(Vector3 groundNormal)
    {
        var origin = mover.GetGroundPoint() + (transform.up * 2);
        /*
        #region Debug
        var Draw = Drawing.Draw.ingame;
        
        using (Draw.WithColor(Color.cyan))
        {
            using (Draw.WithLineWidth(2))
            {
                using (Draw.WithDuration(0.05f))
                {
                    Draw.Circle(origin, groundNormal, 0.2f);
                    Draw.Line(origin, origin + walker.cameraTransform.forward * (mover.colliderThickness));
                }
            }
        }
        #endregion
        */
        var velocity = walker.GetMovementVelocity();
        if (velocity == Vector3.zero) return false;
        
        if (Physics.Raycast(origin, velocity,
            out RaycastHit hitInfo, 1 + 0.5f))
        {
            if (Vector3.Angle(groundNormal, hitInfo.normal) > walker.slopeLimit)
            {
                StartCoroutine(LerpToWall(hitInfo.point, hitInfo.normal));
                return true;
            }
        }
        return false;
    }

    private Quaternion GetRotationFromNormal(Vector3 normal)
    {
        var forward = Vector3.Cross(transform.up, normal);
        return Quaternion.LookRotation(forward, normal);
    }

    private IEnumerator LerpToWall(Vector3 point, Vector3 normal)
    {
        switchingWall = true;

        rb.isKinematic = true;
        //this.enabled = false;
        //col.enabled = false;
        walker.enabled = false;
        mover.enabled = false;
        
        var percentage = 0f;
        var startPosition = rb.position;
        var startRotation = rb.rotation;
        while (percentage < 1)
        {
            Lerp(percentage);
            percentage = Mathf.Clamp(percentage + Time.deltaTime / changeSurfaceDuration, 0, 1);
            yield return null;
        }
        Lerp(percentage);

        rb.isKinematic = false;
        this.enabled = true;
        //col.enabled = true;
        walker.enabled = true;
        mover.enabled = true;

        switchingWall = false;
        
        void Lerp(float progress)
        {
            transform.position = Vector3.Lerp(startPosition, point, progress);
            var yRot = rb.rotation.eulerAngles.y; 
            var newRot = (Quaternion.Lerp(startRotation, GetRotationFromNormal(normal), progress)).eulerAngles;
            rb.rotation = Quaternion.Euler(newRot.x, yRot, newRot.z);
        }
    }
}
