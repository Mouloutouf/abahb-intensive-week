using System;
using System.Collections;
using System.Collections.Generic;
using CMF;
using Sirenix.OdinInspector;
using UnityEngine;

public class WallClimbing : MonoBehaviour
{
    [FoldoutGroup("Settings"), SerializeField] 
    private float yOffsetToCenter = 1;
    [FoldoutGroup("Settings"), SerializeField]
    private float keyHeldTime = 0.2f;
    
    private Mover mover;
    private AdvancedWalkerController controller;
    private Character2DInput input;
    private Rigidbody rb;

    private ControllerMode mode = ControllerMode.Ground;

    private bool climbing = false;
    private bool climbed = true;
    private float climbDelay = 0.2f;
    private float climbCooldown;
    [ShowInInspector, HideInEditorMode]
    private DirectionalKeys keys = new DirectionalKeys();

    private float vertical;
    private float horizontal;
    private float prevVertical = 0;
    private float prevHorizontal = 0;

    private void Start()
    {
        TryGetComponent(out input);
        TryGetComponent(out rb);
        TryGetComponent(out mover);
        TryGetComponent(out controller);
    }
    void HandleInput()
    {
        prevVertical = vertical;
        prevHorizontal = horizontal;
        vertical = input.Vertical();
        horizontal = input.Horizontal();
        keys.Evaluate(vertical, horizontal);
    }

    private void Update()
    {
        HandleInput();
        CheckForGravityChange();
    }

    private void CheckForGravityChange()
    {
        if(climbing) return;
        if (climbCooldown != 0)
        {
            climbCooldown = Mathf.Clamp(climbCooldown - Time.deltaTime, 0, climbDelay);
            if(climbCooldown != 0) return;
        }
        var grounded = controller.IsGrounded();
        switch (mode)
        {
            case ControllerMode.Ground:
                if (keys.up.state == KeyState.On)
                {
                    if(CheckForWall(Vector3.forward)) SwitchMode(ControllerMode.Background);
                }

                if ((keys.left.state == KeyState.On) || (keys.right.state == KeyState.On))
                {
                    if (CheckForWall(new Vector3(Mathf.Ceil(horizontal), 0, 0))) SwitchMode(horizontal > 0 ? ControllerMode.SideRight : ControllerMode.SideLeft);
                }
                break;
            case ControllerMode.Background:
                if (keys.down.state == KeyState.On)
                {
                    if(CheckForWall(Vector3.down)) SwitchMode(ControllerMode.Ground);
                }

                if ((keys.left.state == KeyState.On) || (keys.right.state == KeyState.On))
                {
                    if (CheckForWall(new Vector3(Mathf.Ceil(horizontal), 0, 0))) SwitchMode(horizontal > 0 ? ControllerMode.SideRight : ControllerMode.SideLeft);
                }
                break;
            case ControllerMode.SideLeft:
                if (keys.up.state == KeyState.On && CheckForWall(Vector3.forward))
                {
                    SwitchMode(ControllerMode.Background);
                }
                else if (keys.right.state == KeyState.On && CheckForWall(Vector3.down))
                {
                    SwitchMode(ControllerMode.Ground);
                }
                else if (!grounded)
                {
                    if(!CheckForGround(Vector3.left)) SwitchMode(ControllerMode.Ground);
                }
                else
                {
                    rb.MoveRotation(Quaternion.Lerp(rb.rotation, GetRotationFromNormal(mover.GetGroundNormal()), 10 * Time.deltaTime));
                }
                break;
            case ControllerMode.SideRight:
                if (keys.up.state == KeyState.On && CheckForWall(Vector3.forward))
                {
                    SwitchMode(ControllerMode.Background);
                }
                else if (keys.left.state == KeyState.On && CheckForWall(Vector3.down))
                {
                    SwitchMode(ControllerMode.Ground);
                }
                else if (!grounded)
                {
                    if(!CheckForGround(Vector3.right)) SwitchMode(ControllerMode.Ground);
                }
                else
                {
                    rb.MoveRotation(Quaternion.Lerp(rb.rotation, GetRotationFromNormal(mover.GetGroundNormal()), 10 * Time.deltaTime));
                }
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private bool CheckForWall(Vector3 direction)
    {
        var origin = transform.position + transform.up * yOffsetToCenter;
        var distance = direction.z != 0 ? 1.05f : 0.55f;
        if (Physics.Raycast(origin, direction, out RaycastHit hitInfo, distance))
        {
            //Debug.Log($"[Physics] : Wall in {direction} found");
            //Debug.DrawRay(origin, direction, Color.green, 2f);
            return true;
        }
        //Debug.Log($"[Physics] : Wall in {direction} not found");
        //Debug.DrawRay(origin, direction, Color.red);
        return false;
    }
    private bool CheckForGround(Vector3 direction)
    {
        var origin = transform.position;
        if (Physics.Raycast(origin, direction, out RaycastHit hitInfo, 1.05f))
        {
            //Debug.Log($"[Physics] : Wall in {direction} found");
            //Debug.DrawRay(origin, direction, Color.green, 2f);
            return true;
        }
        //Debug.Log($"[Physics] : Wall in {direction} not found");
        //Debug.DrawRay(origin, direction, Color.red);
        return false;
    }

    private void SwitchMode(ControllerMode newMode)
    {
        if (mode == newMode) return;
        switch (newMode)
        {
            case ControllerMode.Ground:
                SwitchGravity(Vector3.zero);
                input.inverse = false;
                input.axises = Character2DInput.Movement.Horizontal;
                rb.transform.position = new Vector3(rb.transform.position.x, rb.transform.position.y, 0);
                break;
            case ControllerMode.Background:
                SwitchGravity(Vector3.left * 90);
                input.inverse = false;
                input.axises = Character2DInput.Movement.Horizontal | Character2DInput.Movement.Vertical;
                rb.transform.position = new Vector3(rb.transform.position.x, rb.transform.position.y, 1);
                break;
            case ControllerMode.SideLeft:
                SwitchGravity(Vector3.back * 90);
                input.inverse = false;
                input.axises = Character2DInput.Movement.Horizontal;
                rb.transform.position = new Vector3(rb.transform.position.x, rb.transform.position.y, 0);
                break;
            case ControllerMode.SideRight:
                SwitchGravity(Vector3.forward * 90);
                input.inverse = false;
                input.axises = Character2DInput.Movement.Horizontal;
                rb.transform.position = new Vector3(rb.transform.position.x, rb.transform.position.y, 0);
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(newMode), newMode, null);
        }
        mode = newMode;
    }

    private void SwitchGravity(Vector3 eulers)
    {
        StartCoroutine(LerpRotation(eulers));
    }
    private IEnumerator LerpRotation(Vector3 eulers)
    {
        if (controller.currentControllerState != AdvancedWalkerController.ControllerState.Rising)
        {
            input.freeze = climbing = true;
            controller.enabled = mover;
            mover.SetVelocity(Vector3.zero);
            float percentage = 0;
            float timeToClimb = 0.35f;
            Quaternion originalRot = rb.rotation;
            Quaternion destinationRot = Quaternion.Euler(eulers);
            while (percentage < 1)
            {
                rb.MoveRotation(Quaternion.Lerp(originalRot, destinationRot, percentage));
                percentage = Mathf.Clamp(percentage + Time.deltaTime / timeToClimb, 0, 1);
                yield return null;
            }
            rb.MoveRotation(destinationRot);
            input.freeze = climbing = false;
            controller.enabled = true;
            climbed = false;
            climbCooldown = climbDelay;
        }
        
        else
        {
            rb.MoveRotation(Quaternion.Euler(eulers));
        }
    }
    
    private Quaternion GetRotationFromNormal(Vector3 normal)
    {
        var forward = Vector3.Cross(transform.right, normal);
        return Quaternion.LookRotation(forward, normal);
    }
    public enum ControllerMode
    {
        Ground,
        Background,
        SideLeft,
        SideRight,
    }
}
