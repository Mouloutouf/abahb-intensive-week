using System.Collections;
using System.Collections.Generic;
using CMF;
using UnityEngine;

public class GrappleBehaviour : MonoBehaviour
{
    public Controller controller;

    /// Line Rendering
    public LineRenderer lineRenderer;
    public float lineSpeed;

    /// Hook
    public Transform hookTr;

    /// Hook Settings
    public float distance;

    public float grabRadius;
    public LayerMask grabMask;

    public float hitRadius;
    public LayerMask hitMask;

    public float retrieveWaitTime;

    /// References
    /// Input Managers
    public GrappleInput grappleInput;
    public CharacterClimberInput characterInput;
    /// Character
    public Transform model;
    public Rigidbody characterRb;

    /// Input Values
    private bool grappleKeyPressed;
    private bool grappleKeyReleased;
    private Vector2 currentDirection;

    /// Positions
    private Vector2 localHookPosition;
    private Vector3 worldHookPosition;
    private Vector2 hookPosition;

    /// Grab
    private bool grabMode { get => currentGrabbed != null; }
    private bool thrown;
    private Grabbable currentGrabbed; public Grabbable CurrentGrabbed {
        get => currentGrabbed;
        set => currentGrabbed = value;
    }

    void CheckForInput()
    {
        grappleKeyPressed = grappleInput.IsGrappleKeyPressed();
        grappleKeyReleased = grappleInput.IsGrappleKeyReleased();
    }
    void CheckForDirection()
    {
        currentDirection = new Vector2(characterInput.Horizontal(), characterInput.Vertical());
    }

    private void Update()
    {
        CheckForInput();

        if (grabMode == false)
        {
            worldHookPosition = transform.position + (Vector3)hookPosition;
            UpdateLine(worldHookPosition);

            if (grappleKeyPressed && thrown == false)
            {
                CheckForDirection();
                if (currentDirection == Vector2.zero) currentDirection = model.forward;
                ThrowGrapple(currentDirection.normalized);
                //Debug.LogWarning("yes throw " + currentDirection);
            }

            if (hookTr == null) return;
            hookTr.position = worldHookPosition;
            hookTr.LookAt(worldHookPosition);
        }
        else if (grabMode == true)
        {
            worldHookPosition = currentGrabbed.transform.position;
            UpdateLine(worldHookPosition);

            if (grappleKeyPressed)
            {
                currentGrabbed.Detach();
            }

            if (hookTr == null) return;
            hookTr.position = worldHookPosition;
            hookTr.LookAt(worldHookPosition);
        }
    }
    void UpdateLine(Vector3 _worldHookPosition)
    {
        lineRenderer.SetPosition(0, transform.position);
        lineRenderer.SetPosition(1, _worldHookPosition);
    }

    void ThrowGrapple(Vector2 _direction)
    {
        thrown = true;
        controller.OnThrow.Invoke(_direction);

        localHookPosition = _direction * distance;

        hookPosition = Vector2.zero;
        StartCoroutine(UpdateThrow());
    }
    IEnumerator UpdateThrow()
    {
        for (float ft = 0f; ft <= 1; ft += Time.deltaTime * lineSpeed)
        {
            hookPosition = Vector2.Lerp(Vector2.zero, localHookPosition, ft);
            
            yield return new WaitForEndOfFrame();
        }
        HookCheck();
    }

    private void HookCheck()
    {
        worldHookPosition = transform.position + (Vector3)hookPosition;

        Vector3 hookStartPosition = transform.position + (Vector3)hookPosition * 0.2f;

        #region Gizmos
        gizmoHookStartPos = hookStartPosition;
        gizmoHookWorldPos = worldHookPosition;
        hookGizmosDisplay = true;
        StartCoroutine(StopGizmos());
        #endregion

        bool singleGrab = false;
        RaycastHit[] grabRaycastHits = Physics.CapsuleCastAll(hookStartPosition, worldHookPosition, grabRadius, worldHookPosition, distance, grabMask);

        if (grabRaycastHits.Length > 0 && singleGrab == false) {
            foreach (var hit in grabRaycastHits)
            {
                Collider col = hit.collider;

                col.gameObject.TryGetComponent(out Grabbable grabbable);
                if (currentGrabbed != null) currentGrabbed.Detach();
                grabbable.Attach(this);

                singleGrab = true;
            }
        }

        bool singleHit = false;
        RaycastHit[] hitRaycastHits = Physics.CapsuleCastAll(hookStartPosition, worldHookPosition, hitRadius, worldHookPosition, distance, hitMask);

        if (hitRaycastHits.Length > 0 && singleHit == false) {
            foreach (var hit in hitRaycastHits)
            {
                Collider col = hit.collider;

                col.gameObject.TryGetComponent(out HitBehaviour hitBehaviour);
                Debug.LogWarning("yes hit");
                hitBehaviour.OnHit();

                singleHit = true;
            }
        }

        if (grabMode == false)
        {
            StartCoroutine(WaitRetrieve(retrieveWaitTime));
        }
    }

    IEnumerator WaitRetrieve(float time)
    {
        yield return new WaitForSeconds(time);
        RetrieveGrapple();
    }
    public void RetrieveGrapple()
    {
        localHookPosition = worldHookPosition - transform.position;
        StartCoroutine(UpdateRetrieve());
    }
    IEnumerator UpdateRetrieve()
    {
        for (float ft = 1f; ft >= 0; ft -= Time.deltaTime * lineSpeed)
        {
            hookPosition = Vector2.Lerp(Vector2.zero, localHookPosition, ft);
            yield return new WaitForEndOfFrame();
        }

        thrown = false;
    }

    // This is some real shit gizmos code, don't mind it
    #region Gizmos
    Vector3 characterPos;
    Vector3 hookGizmoPos;

    Vector3 gizmoHookStartPos;
    Vector3 gizmoHookWorldPos;
    bool hookGizmosDisplay;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        characterPos = transform.position;
        hookGizmoPos = characterPos + Vector3.right * distance;
        Gizmos.DrawLine(characterPos, hookGizmoPos);

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(hookGizmoPos, grabRadius);

        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(hookGizmoPos, hitRadius);

        if (hookGizmosDisplay)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(gizmoHookStartPos, grabRadius);
            Gizmos.DrawLine(gizmoHookStartPos, gizmoHookWorldPos);
            Gizmos.DrawWireSphere(gizmoHookWorldPos, grabRadius);

            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(gizmoHookStartPos, hitRadius);
            Gizmos.DrawLine(gizmoHookStartPos, gizmoHookWorldPos);
            Gizmos.DrawWireSphere(gizmoHookWorldPos, hitRadius);
        }
    }
    IEnumerator StopGizmos()
    {
        yield return new WaitForSeconds(retrieveWaitTime);
        hookGizmosDisplay = false;
    }
    #endregion
}
