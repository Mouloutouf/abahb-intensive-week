using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum JointType { FixedJoint, SpringJoint, HingeJoint, CharacterJoint, ConfigurableJoint }

public abstract class Grabbable : MonoBehaviour
{
    public GameObject parent;

    public Joint joint;

    private GrappleBehaviour connectedGrapple;

    public bool useEvent;
    [ShowIf("useEvent")]
    public UnityEvent grabEvent;

    protected virtual void Start()
    {
        Detach();
    }

    public virtual void Attach(GrappleBehaviour grapple)
    {
        connectedGrapple = grapple;
        connectedGrapple.CurrentGrabbed = this;

        joint.connectedBody = grapple.characterRb;

        grabEvent.Invoke();
    }
    public virtual void Detach()
    {
        joint.connectedBody = null;
        
        if (connectedGrapple == null) return;
        connectedGrapple.CurrentGrabbed = null;
        connectedGrapple.RetrieveGrapple();

        connectedGrapple = null;
    }

    protected virtual void OnJointBreak(float breakForce)
    {
    }
}