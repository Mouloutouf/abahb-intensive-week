using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Respawn respawn;
    public MenuInput menuInput;
    public EndScreen endScreen;

    private void Start()
    {
        RestartGame();
    }

    public void QuitGame() => Application.Quit();

    public void RestartGame() { respawn.RespawnPlayer(); ResumeGame(); }

    public void ResumeGame() { menuInput.OpenMenu(false); endScreen.OpenEndScreen(false); }
}
