using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MoreMountains.Feedbacks;

public class EndScreen : MonoBehaviour
{
    public Transform endscreenUI;

    public MMFeedbacks feedbacks;

    private void OnTriggerEnter(Collider other)
    {
        OpenEndScreen(true);
        feedbacks.PlayFeedbacks();
    }

    public void OpenEndScreen(bool state)
    {
        endscreenUI.gameObject.SetActive(state);
    }
}
