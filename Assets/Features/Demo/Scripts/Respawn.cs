using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour
{
    public Transform spawnPoint;

    public LayerMask playerLayer;
    public Transform player;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerUtility.playerLayer)
        {
            RespawnPlayer();
        }
    }

    public void RespawnPlayer()
    {
        player.position = spawnPoint.position;
    }
}
