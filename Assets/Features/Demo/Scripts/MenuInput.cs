using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuInput : MonoBehaviour
{
    public Transform pauseMenuUI;

    private bool keyMenuPressed;
    private bool menuIsOpen;

    private void Update()
    {
        keyMenuPressed = IsMenuKeyPressed();

        if (keyMenuPressed)
        {
            if (menuIsOpen)
            {
                OpenMenu(false);
            }
            else
            {
                OpenMenu(true);
            }
        }
    }

    public void OpenMenu(bool state)
    {
        pauseMenuUI.gameObject.SetActive(state);
        menuIsOpen = state;
    }

    public bool IsMenuKeyPressed()
    {
        return Input.GetKeyDown(KeyCode.Escape);
    }
}
